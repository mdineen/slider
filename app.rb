require 'sinatra'
require 'json'
require 'aws-sdk'



class Application < Sinatra::Base
	#use Rack::Session::Cookie
	
	

	get '/' do
		@images = listImages
		erb :touch
	end

	def listImages
		a = Array.new
		s3 = AWS::S3.new(:access_key_id => ENV['AWS_ACCESS_KEY'],
						 :secret_access_key => ENV['AWS_SECRET_KEY'])
		bucket = s3.buckets[ENV['S3_BUCKET']]
			bucket.objects.each do |o|
				a.push o.public_url(:secure => :false)
			end
	a
	end

end